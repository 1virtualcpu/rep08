# 今日のフィーリングで作ったサイト

このコードはMITライセンスで公開しています。
自由に流用していただいてOKです。

## 動作確認の仕方

npmを実行できる環境で次のコマンドを実行します

```
cd ~
git clone https://gitlab.com/1virtualcpu/rep08.git
cd rep08
npm install -y
```

parcelを実行します。

```
npx parcel index.html
```

ブラウザを起動してhttp://localhost:1234へ接続します
